package ru.t1consulting.nkolesnik.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    List<ProjectDTO> findAll(@Nullable Sort sort);

    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    List<ProjectDTO> findAll(@Nullable Comparator comparator);

    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

}
