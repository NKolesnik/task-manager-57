package ru.t1consulting.nkolesnik.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class FileScanner {

    @NotNull
    private final File folder = new File("./");
    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    @Autowired
    public Bootstrap bootstrap;
    @NotNull
    @Autowired
    public ApplicationEventPublisher eventPublisher;
    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @SneakyThrows
    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean checkName = listeners.contains(fileName);
            if (checkName) {
                file.delete();
                eventPublisher.publishEvent(new ConsoleEvent(fileName));
            }
        }
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

}
