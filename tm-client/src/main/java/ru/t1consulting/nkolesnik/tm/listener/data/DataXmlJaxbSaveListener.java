package ru.t1consulting.nkolesnik.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataXmlJaxbSaveRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;

@Component
public final class DataXmlJaxbSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-jaxb-xml";

    @NotNull
    public static final String DESCRIPTION = "Save data to xml file using JAXB.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlJaxbSaveListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlJaxbSaveRequest(getToken()));
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
