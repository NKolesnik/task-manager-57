package ru.t1consulting.nkolesnik.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserUpdateRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserUpdateResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ENTER FIRST NAME]");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME]");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME]");
        @NotNull final String lastName = TerminalUtil.nextLine();
        @NotNull final UserUpdateRequest request = new UserUpdateRequest(getToken());
        request.setFirstName(firstName);
        request.setMiddleName(middleName);
        request.setLastName(lastName);
        @NotNull final UserUpdateResponse response = getUserEndpoint().updateUser(request);
        @Nullable final UserDTO user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
